﻿using Calculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace myTestsnew
{
    public class Calculatortest
    {
        //[Theory]
        [Fact]
        public void Add_ifthenumbersareaddedcorrectly()
        {
            double actual = Calculatorclass.Add(1, 4);

            Assert.Equal(5, actual);
        }
        [Fact]
        public void Sutract_ifthenumbersaresubtractedcorrectly()
        {
            double actual = Calculatorclass.Subtract(1, 4);

            Assert.Equal(-3, actual);
        }
        [Fact]
        public void Multiply_ifthenumbersaremultilpiedcorrectly()
        {
            double actual = Calculatorclass.Multiply(1, 4);

            Assert.Equal(4, actual);
        }
        [Fact]
        public void Divide_ifthenumbersaredividedcorrectly()
        {
            double actual = Calculatorclass.Divide(1, 4);

            Assert.Equal(0.25, actual);
        }
        [Fact]
        public void Divide_ifdividebyzero()
        {
            double actual = Calculatorclass.Divide(2,0);

            Assert.Equal(0, actual);
        }
        [Fact]
        public void Subtract_ifdoublenegativnumber()
        {
            double actual = Calculatorclass.Subtract(-1, -4);

            Assert.Equal(3, actual);
        }
        [Fact]
        public void Add_ifmathworks()
        {
            double actual = Calculatorclass.Add(Math.Cos(0), Math.Sin(0));

            Assert.Equal(1, actual);
        }
        [Fact]
        public void Divide_dividezerowithitself()
        {
            double actual = Calculatorclass.Divide(0, 0);

            Assert.Equal(1, actual);
        }
    }
}
